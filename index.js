/**
 * Code Assistant File System manipulator
 * 
 * Provides API for manipulating files
 * 
 * check: code-assist-hyper-app
 */
const fs = require('fs-extra');
const fastify = require('fastify');
const app = fastify({ logger: true });

app.register(require('fastify-cors'), {
  // put your options here
});

app.get('/folder', (req, reply) => {
  return fs.readdirSync(req.query.name);
});

app.get('/file', (req, reply) => {
  return fs.readFilesync(req.query.name, 'utf8');
});

app.put('/file', (req, reply) => {
  // return await fs.readFile(req.query.name, 'utf8');
  const flags = { flag: 'w+' };
  const content = req.body.content; //JSON.stringify(req.body.content, null, 2);
  return fs.writeFileSync(req.body.filename, content, flags);
});

// Run the server!
const PORT = 3001;
const start = async () => {
  try {
    await app.listen(PORT).then(() => {
      console.log(`Server running at http://localhost:${PORT}/`);
    });
  } catch (err) {
    app.log.error(err)
    process.exit(1);
  }
}

start();
